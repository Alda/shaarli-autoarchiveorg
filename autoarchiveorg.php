<?php

/*
 * Automatically save shared article on archive.org
 */
function hook_autoarchiveorg_save_link ($data) {
    $wayback = new AutoArchive_WaybackMachine($data['url']);

    (new AutoArchive_CurlRunner([$wayback]))->run();

    return $data;
}

interface AutoArchive_CurlArchiver {
    public function __construct ($url);
    public function getCurl ();
}

class AutoArchive_WaybackMachine {
    const WAYBACK_URL = 'https://web.archive.org/save/';
    const CURLOPT = [
        CURLOPT_NOBODY => true,
        CURLOPT_RETURNTRANSFER => true
    ];

    private $url;

    public function __construct ($url) {
        $this->url = $url;
    }

    public function getCurl () {
        $curl = curl_init();
        $options = self::CURLOPT + [CURLOPT_URL => self::WAYBACK_URL . $this->url];

        curl_setopt_array($curl, $options);

        return $curl;
    }
}

class AutoArchive_CurlRunner {
    private $archiverList;

    public function __construct(array $curlArchiverList) {
        $this->archiverList = array_map(function ($archiver) {
            return $archiver->getCurl();
        }, $curlArchiverList);
    }

    public function run() {
        foreach ($this->archiverList as $curl) {
            curl_exec($curl);
        }
    }
}
